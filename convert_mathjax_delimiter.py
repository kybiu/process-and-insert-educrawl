import pandas as pd
from os import listdir
from helper.object_storage import ObjectStorage
import requests
import base64
from bs4 import BeautifulSoup as BS
import re

not_good_delimiter = ["\(", "\)", "\[", "\]"]
good_delimiter = ["$"]
answer_options = ["A. ", "B. ", "C. ", "D. ", "E. ", "F. ", "G. ", "H. ", "I. ", "J. ", "K. ", "M. "]
image_dict = {}


def cleanhtml(raw_html):
    cleantext = BS(raw_html, "lxml").text
    return cleantext


def get_dir():
    data_test_path = "data/hoctot/"
    dir_list = [f for f in listdir(data_test_path)]
    return dir_list


def get_files(dir):
    file_path = "data/hoctot/" + dir
    file_list = [f for f in listdir(file_path)]
    return file_list


def convert_delimiter(dir=None, file=None):
    if dir:
        file_path = "data/hoctot/" + dir + "/" + file
    else:
        file_path = "data/hoctot/" + file

    df = pd.read_csv(file_path, lineterminator='\n')

    df = df[~df["question_number"].isna()]
    question = [x.replace("\(", "$").replace("\)", "$").replace("\]", "$").replace("\[", "$") if str(x) != "nan" else x for x in df["question"]]
    answers = [x.replace("\(", "$").replace("\)", "$").replace("\]", "$").replace("\[", "$") if str(x) != "nan" else x for x in df["answers"]]
    correct_answer_text = [x.replace("\(", "$").replace("\)", "$").replace("\]", "$").replace("\[", "$") if str(x) != "nan" else x for x in df["correct_answer_text"]]
    explain = [x.replace("\(", "$").replace("\)", "$").replace("\]", "$").replace("\[", "$") if str(x) != "nan" else x for x in df["explain"]]
    explain = [x.strip() if str(x) != "nan" else x for x in explain]

    correct_answer_id_list = df["correct_answer_id"].to_list()
    for index, correct_answer_id in enumerate(correct_answer_id_list):
        correct_answer_id = correct_answer_id.replace("[", "").replace("]", "")
        if correct_answer_id:
            correct_answer_id = int(correct_answer_id.split(",")[0])
        else:
            continue

        if correct_answer_text[index]:
            if answer_options[correct_answer_id] not in str(correct_answer_text[index]):
                correct_answer_text[index] = answer_options[correct_answer_id] + correct_answer_text[index].strip()
            else:
                correct_answer_text[index] = correct_answer_text[index].strip()

    new_answers = []
    for row_index, row in enumerate(answers):
        if str(row) != "nan":
            row = row.replace(" \n ", "\n").replace("\n ", "\n").replace(" \n", "\n")
            row = row.replace(" \r\n ", "\r\n").replace("\r\n ", "\r\n").replace(" \r\n", "\r\n")
            row = row.replace("\r\n</u>", "</u>").replace("\n</u>", "</u>")
            row = row.replace("\r\n </u>", "</u>").replace("\n </u>", "</u>")
            use_regex = False

            lines_count = row.count("\n")
            if "data:image" in row:
                row = row.replace("data:image", "@!#data:image")
            elif lines_count == 3:
                row = row.replace("\r\n", "@!#")
                row = row.replace("\n", "@!#")
            elif "$\n" in row or "$\r\n" in row:
                row = row.replace("$\r\n", "$@!#")
                row = row.replace("$\n", "$@!#")
            elif "$.\n" in row or "$.\r\n" in row:
                row = row.replace("$.\r\n", "$.@!#")
                row = row.replace("$.\n", "$.@!#")
            elif "\n" in row and "\r\n" in row:
                use_regex = True
            else:
                row = row.replace("\n", "@!#")
            if use_regex:
                answer_list = re.split("(?<!\r)\n", row)
                answer_list = [x.replace("\r\n", " ") for x in answer_list]
            else:
                answer_list = row.split("@!#")

            answer_list = [x.strip() for x in answer_list]
            answer_list = [x for x in answer_list if x != '']
            try:
                answer_list = [answer_options[i] + str(x) if answer_options[i] not in str(x)[:2] else str(x) for i, x in enumerate(answer_list)]
            except Exception as e:
                print(e)
                print("error")
            answer_list = "\n".join(answer_list)
            new_answers.append(answer_list)
        else:
            new_answers.append(row)

    df["question"] = question
    df["answers"] = new_answers
    df["correct_answer_text"] = correct_answer_text
    df["explain"] = explain

    return df


def process_image(df):
    img_list = []
    for item in df.itertuples():
        index = item.Index
        img = item.img
        if str(img) == "nan" or "data" not in str(img):
            img_list.append(None)
            continue
        question_number = item.question_number
        img = base64.b64decode(img[22:])
        with open("data/image/" + str(question_number) + ".png", "wb") as fh:
            fh.write(img)
        img = "data/image/" + str(question_number) + ".png"
        if question_number not in image_dict.keys():
            object_storage = ObjectStorage("v0.2/onluyen", img, img)
            object_storage.post_file()
            minio_url = object_storage.file_name
            image_dict[question_number] = minio_url
        else:
            minio_url = image_dict[question_number]
        img_list.append(minio_url)
    df["img"] = img_list
    return df


def merge_old_new_file():
    df1 = pd.read_csv("data/VATLY_C12_ver3.csv")
    df2 = pd.read_csv("data/practice_old2/12/VATLY_C12.csv")

    df1.insert(1, column="fill_blank_question", value=[None] * len(df1))
    df1 = df1.rename(columns={"id": "question_id"})

    df = pd.concat([df1, df2])
    df = df.drop_duplicates(subset=["question_number"])
    all_answers = df["answers"].to_list()
    new_all_answers = []
    for x in all_answers:
        if x:
            x = "".join([s for s in x.strip().splitlines(True) if s.strip()])
        new_all_answers.append(x)
    df["answers"] = new_all_answers
    df.to_csv("data/practice/12/VATLY_C12.csv", index=False)
    a = 0

def convert_hoctot():
    dirs = get_dir()
    for file in dirs:
        df = convert_delimiter(None, file)
        df.to_csv("data/hoctot_processed/" + file, index=False)


def main():
    dirs = get_dir()
    for dir in dirs:
        file_list = get_files(dir)
        for file in file_list:
            df = convert_delimiter(dir, file)
            df.to_csv("data/hoctot_processed/" + dir + "/" + file, index=False)


if __name__ == '__main__':
    convert_hoctot()
    # merge_old_new_file()
