import pandas as pd
from os import listdir
import requests
import json
import re

subject_dict = {
    "TOAN": "Math",
    "VATLY": "Physics",
    "HOAHOC": "Chemistry",
    "ANHTHPT2": "English",
    "TIENGANHTHPT3": "English",
    "NGUVAN": "Literature",
    "LICHSU": "History",
    "SINH": "Biology",
    "DIALY": "Geography",
    "GDCD": "GDCD",
}


def post_request(data):
    url = "http://192.168.1.21:7000/store/assignment/"
    headers = {'Content-type': 'application/json'}
    a = 0
    response = requests.post(
        url,
        # json=data,
        data=data,
        headers=headers
    )

    try:
        response_text = json.loads(response.text)
        print(str(response.status_code))
    except Exception as e:
        print('Error {} when insert data to database'.format(str(e)))


def get_dir(root_path):
    data_test_path = root_path
    dir_list = [f for f in listdir(data_test_path)]
    return dir_list


def get_files(root_path, dir):
    file_path = root_path + dir
    file_list = [f for f in listdir(file_path)]
    return file_list


def insert_test_to_db(root_path, dir, file):
    source_detail = "test"
    file_path = root_path + dir + "/" + file

    df = pd.read_csv(file_path)
    all_test_id = df[df["test_id"].notna()]["test_id"].drop_duplicates().to_list()
    for test_id in all_test_id:
        sub_df = df[df["test_id"] == test_id].reset_index(drop=True)
        if "storage_id" not in sub_df.columns:
            storage_id = 'None'
        elif len(sub_df["storage_id"].dropna()) != 0:
            storage_id = sub_df["storage_id"].dropna().drop_duplicates().unique()[0]
        else:
            storage_id = 'None'
        subject = subject_dict[sub_df["subject"].drop_duplicates().iloc[0]]
        grade_level = sub_df["grade"].drop_duplicates().iloc[0][1:]
        title = sub_df["test_title"].drop_duplicates().iloc[0]
        test_set_title = sub_df["test_set_title"].drop_duplicates().iloc[0]

        previous_question_name = ""
        previous_fill_blank_question_content = ""

        question_list = []
        for item in sub_df.itertuples():
            item_index = item.Index
            name = 'Câu {index}'.format(index=item_index + 1)

            fill_blank_question = str(item.fill_blank_question) if str(item.fill_blank_question) != "nan" else ""
            if fill_blank_question:
                if fill_blank_question != previous_fill_blank_question_content:
                    previous_fill_blank_question_content = fill_blank_question

                    if previous_question_name:
                        previous_question_name_index = int(previous_question_name[4:])
                        current_question_name_index = previous_question_name_index + 1
                    else:
                        current_question_name_index = int(name[4:])
                    name = 'Câu {index}'.format(index=current_question_name_index)

                else:
                    name = previous_question_name

            img = item.img if item.img else ""
            question = item.question if item.question else ""
            if fill_blank_question:
                question = fill_blank_question + "\n" + question

            text_content = str(question) + " " + str(img)
            text_content = text_content.replace("nan ", "").replace(" nan", "")

            text_answer = str(item.answers) if item.answers else ""
            correct_answer = str(item.correct_answer_text) if item.correct_answer_text else ""
            if correct_answer == "nan" and subject != "Literature":
                continue
            elif correct_answer != "nan" and subject != "Literature":
                correct_answer_id = int(item.correct_answer_id.strip('][')[:1]) if item.correct_answer_id else ""
                if text_answer and correct_answer_id and 'png' in correct_answer:
                    answer_list = text_answer.split("\n")
                    answer_list = [x for x in answer_list if x != '']
                    correct_answer = answer_list[correct_answer_id]
            elif correct_answer == "nan" and subject == "Literature":
                text_answer = ""
                correct_answer = ""

            solution = str(item.explain) if item.explain else ""
            source_crawler = "https://app.onluyen.vn/"
            source_detail = 'test'
            raw_question = "{question};{answers};{correct_answer};{solution}".format(question=text_content, answers=text_answer, correct_answer=correct_answer, solution=solution)

            previous_question_name = name

            question_dict = {
                'name': name,
                'raw_question': raw_question,
                'text_content': text_content,
                'text_answer': text_answer,
                'correct_answer': correct_answer,
                'solution': solution,
                'source_crawler': source_crawler,
                'question_type': '',
            }
            question_list.append(question_dict)

        test_data = {
            'grade_level': grade_level,
            'subject': subject,
            'chapter': '',
            'lesson': '',
            'title': "onluyen_" + title,
            'set_title': test_set_title,
            'source_crawler': "https://app.onluyen.vn/",
            'storage_id': str(storage_id),
            'meta_data': question_list
        }
        test_data_json = json.dumps(test_data)
        with open("output2.txt", "a") as f:
            f.write(test_data_json)
            f.write("\n")
        post_request(test_data_json)


def main():
    root_path = "data/test_processed_with_img/"
    dirs = get_dir(root_path)
    for dir in dirs:
        file_list = get_files(root_path, dir)
        for file in file_list:
            # if file != "test_VATLY_C10.csv":
            #     continue
            insert_test_to_db(root_path, dir, file)


if __name__ == '__main__':
    main()
